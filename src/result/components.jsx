import React from 'react'
import '../css/result.css'
import moment from 'moment'
import { Table, Button, Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { Link } from 'react-router'
import { LineChartResult, GroupLineChartResult } from '../utils/components/chart-component'
import { saveResult, deleteDatabase } from '../utils/database-function'
import WarningModal from '../utils/components/warning-modal'
import SuccessModal from '../utils/components/success-modal'
import { exportPDF } from '../pdf/export-pdf'
import { printPDF } from '../pdf/print-pdf'
import { tooltip } from '../utils/components/tooltip'

export const ResultPage = (props) => {
    const { 
        actions, 
        resultData, 
        averageQuantity, 
        averageMinimum, 
        averageMaximum, 
        averageAverage, 
        isFromCalculate,
        machineChoice, 
        warningModalActions,
        successModalActions,
        result,
        configuration
    } = props

    return (
        <div id='result-page-container'>
            <div id='panel-container'>
                <Row>
                    <Col md={9}>
                        <h1>Result</h1>
                    </Col>
                    <Col md={3}>
                        <h5>{machineChoice === 'Urinalysis' ? 'Urinalysis Modular System' : 'Automated Hematology System'}</h5>
                        <h5>{configuration.name}</h5>
                        <h5>{configuration.date ? moment(configuration.date).format('MM / DD / YYYY') : configuration.date}</h5>
                    </Col>
                </Row>
                <hr/>
                <Row>
                    <Table striped bordered condensed hover className='printElement'>
                        <thead>
                            <tr>
                                <th>Time ( hours )</th>
                                <th>Quantity ( tubes )</th>
                                <th style={{color:'green'}}>Minimum ( minutes )</th>
                                <th style={{color:'orange'}}>Maximum ( minutes )</th>
                                <th style={{color:'blue'}}>Average ( minutes )</th>
                            </tr>
                        </thead>
                        <tbody>
                            {resultData.map((item, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{item.time}</td>
                                        <td>{item.quantity}</td>
                                        <td style={{color:'green'}}>{item.minimum}</td>
                                        <td style={{color:'orange'}}>{item.maximum}</td>
                                        <td style={{color:'blue'}}>{item.average}</td>
                                    </tr>
                                )
                            })}
                            <tr style={{color:'red'}}>
                                <td>Average</td>
                                <td>{averageQuantity}</td>
                                <td>{averageMinimum}</td>
                                <td>{averageMaximum}</td>
                                <td>{averageAverage}</td>
                            </tr>
                        </tbody>
                    </Table>
                    <div className='printElement' style={{marginTop:'20px'}}>
                        <h3>{machineChoice == 'Automated' ? 'XN-10' : 'UC-3500'}</h3>
                        <div style={{backgroundColor:'white', height:'500px'}}>
                            <LineChartResult resultData={resultData} graphType='each' machine='firstMachine'/>
                        </div>
                    </div>

                    <div className='printElement'>
                        <h3>{machineChoice == 'Automated' ? 'SP-10' : 'UF-5000'}</h3>                
                        <div style={{backgroundColor:'white', height:'500px'}}>
                            <LineChartResult resultData={resultData} graphType='each' machine='secondMachine'/>                
                        </div>
                    </div>

                    <div className='printElement'>
                        <h3>{machineChoice == 'Automated' ? 'DI-60' : 'UD-10'}</h3>                
                        <div style={{backgroundColor:'white', height:'500px'}}>
                            <LineChartResult resultData={resultData} graphType='each' machine='thirdMachine'/>                
                        </div>
                    </div>

                    <div className='printElement'>
                        <h3>Result Graph</h3>
                        <div style={{backgroundColor:'white', height:'500px'}}>
                            <GroupLineChartResult resultData={resultData} graphType='all' machine='all'/>                
                        </div>
                    </div>
                </Row>
            </div>
            <div id='menu-container'>
                <OverlayTrigger placement='left' overlay={tooltip('Back to Home')}>
                    <Button className='menu-button' onClick={() => warningModalActions.showWarningModal()}>
                        <i className='fa fa-home'/>
                    </Button>
                </OverlayTrigger>
                <OverlayTrigger placement='top' overlay={tooltip('Save Result')}>
                    <Button 
                        className='menu-button' 
                        style={{marginLeft:'10%', marginRight:'10%'}}
                        disabled={!isFromCalculate}
                        onClick={() => {saveResult(result, configuration, machineChoice); successModalActions.showSuccessModal()}}>
                        <i className='fa fa-save'/>
                    </Button>
                </OverlayTrigger>
                <OverlayTrigger placement='right' overlay={tooltip('Download Result')}>
                    <Button className='menu-button' onClick={() => exportPDF(configuration)}>
                        <i className='fa fa-download'/>
                    </Button>
                </OverlayTrigger>
                {/* <Button className='menu-button' onClick={() => printPDF()}>
                    <i className='fa fa-print'/>
                </Button> */}
            </div>
            <WarningModal/>
            <SuccessModal/>
        </div>
    )
}