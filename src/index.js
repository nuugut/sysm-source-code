import React from 'react'
import ReactDOM from 'react-dom'
import './css/index.css'
import { store } from './store'
import { Router, Route, browserHistory, IndexRedirect } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { Provider } from 'react-redux'
import HomePage from './home/containers'
import GeneralInfoPage from './general-info/containers'
import ConfigurationPage from './configuration/containers'
import SamplePatternPage from './sample-pattern/containers'
import ResultPage from './result/containers'

import generalInfoActions from './general-info/actions'


const history = syncHistoryWithStore(browserHistory, store)
const state = store.getState()

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route path='/'>
				<IndexRedirect to='/home'/>
                <Route path='home' component={HomePage}/>
                <Route path='general-info' component={GeneralInfoPage}
                    onEnter={() => store.dispatch(generalInfoActions.getAllResultTitle(store.getState().home.machineChoice))}/>
                <Route path='configuration' component={ConfigurationPage}/>
                <Route path='sample-pattern' component={SamplePatternPage}/>
                <Route path='result' component={ResultPage}/>
            </Route>
        </Router>
    </Provider>, 
    document.getElementById('root')
);
