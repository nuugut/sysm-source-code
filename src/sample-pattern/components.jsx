import React from 'react'
import '../css/sample-pattern.css'
import moment from 'moment'
import { FormControl, Table, Button, Row, Col, OverlayTrigger } from 'react-bootstrap'
import { Link } from 'react-router'
import { tooltip } from '../utils/components/tooltip'

export const SamplePatternPage = (props) => {
    const { actions, resultActions, samplePatternData, configuration, machineChoice } = props
    const isAllQuantityFill = () => {
        const isAllFill = samplePatternData.map((value) => {
            return value.quantity
        }).every((value) => {
            return value !== ''
        })
        return isAllFill
    }
    const IsLink = () => {
        if (!isAllQuantityFill()) {
            return (
                <OverlayTrigger placement='right' overlay={tooltip('Please fill all sample pattern')}>
                    <span>
                        <Button 
                            className='menu-button' 
                            style={{pointerEvents:'none'}}
                            disabled={!isAllQuantityFill()} 
                            onClick={() => resultActions.calculateResult(samplePatternData, configuration, machineChoice)}>
                                <i className='fa fa-calculator'/>
                        </Button>
                    </span>
                </OverlayTrigger>
            )
        }
        else {
            return (
                <OverlayTrigger placement='right' overlay={tooltip('Calculate Result')}>
                    <Link to='result'>
                        <Button 
                            className='menu-button' 
                            disabled={!isAllQuantityFill()} 
                            onClick={() => resultActions.calculateResult(samplePatternData, configuration, machineChoice)}>
                                <i className='fa fa-calculator'/>
                        </Button>
                    </Link>
                </OverlayTrigger>
            )
        } 
    }
    return (
        <div id='sample-pattern-page-container'>
            <div id='panel-container'>
                <Row>
                    <Col md={9}>
                        <h1>Sample Pattern</h1>
                    </Col>
                    <Col md={3}>
                        <h5>{machineChoice === 'Urinalysis' ? 'Urinalysis Modular System' : 'Automated Hematology System'}</h5>
                        <h5>{configuration.name}</h5>
                        <h5>{configuration.date ? moment(configuration.date).format('MM / DD / YYYY') : configuration.date}</h5>
                    </Col>
                </Row>
                <hr/>
                <Row style={{display:'flex', justifyContent:'center'}}>
                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Time ( hours )</th>
                                <th>Quantity ( tubes )</th>
                            </tr>
                        </thead>
                        <tbody>
                            {samplePatternData.map((item, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{item.time}</td>
                                        <td>
                                            <FormControl 
                                                type='number'
                                                value={samplePatternData[key].quantity}
                                                onChange={(e) => actions.changeQuantity(e.target.value, key)}/>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </Row>
            </div>
            <div id='menu-container'>
                <OverlayTrigger placement='left' overlay={tooltip('Back to Configuration')}>
                    <Link to='configuration'>
                        <Button className='menu-button'><i className='fa fa-arrow-left'/></Button>
                    </Link>
                </OverlayTrigger>
                <OverlayTrigger placement='top' overlay={tooltip('Reset Input')}>
                    <Button className='menu-button' style={{marginLeft:'10%', marginRight:'10%'}} onClick={() => actions.resetData()}>
                        <i className='fa fa-redo'/>
                    </Button>
                </OverlayTrigger>
                <IsLink/>
            </div>
        </div>
    )
}