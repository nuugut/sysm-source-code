import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { SamplePatternPage } from './components'
import actions from './actions'
import resultActions from '../result/actions'

const mapStateToProps = (state) => {
    return {
        samplePatternData: state.samplePattern.samplePatternData,
        configuration: state.configuration,
        machineChoice: state.home.machineChoice
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch),
        resultActions: bindActionCreators(resultActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SamplePatternPage)