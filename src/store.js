import { createStore, combineReducers, applyMiddleware } from 'redux'
import { browserHistory } from 'react-router'
import { routerMiddleware, routerReducer } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
import home from './home/reducers'
import generalInfo from './general-info/reducers'
import configuration from './configuration/reducers'
import samplePattern from './sample-pattern/reducers'
import result from './result/reducers'
import utils from './utils/reducers'

const middleware = [
    thunkMiddleware,
    logger,
    routerMiddleware(browserHistory)
]


const rootReducers = (state, action) => {
    if (action.type === 'RESET_ALL_DATA') {
        state = undefined
    }
    return appReducers(state, action)
}


const appReducers = combineReducers({
    routing: routerReducer,
    home,
    generalInfo,
    configuration,
    samplePattern,
    result,
    utils
})

export const store = createStore(rootReducers, {}, applyMiddleware(...middleware))