import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { GeneralInfoPage } from './components'
import actions from './actions'
import warningModalActions from '../utils/actions/warning-modal-actions'
import resultActions from '../result/actions'
import removeModalActions from '../utils/actions/remove-modal-actions'

const mapStateToProps = (state) => {
    return {
        resultTitle: state.generalInfo.resultTitle,
        loading: state.generalInfo.loading,
        searchName: state.generalInfo.searchName,
        searchDate: state.generalInfo.searchDate,
        showConfirmModal: state.utils.showConfirmModal.show,
        machineChoice: state.home.machineChoice
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch),
        warningModalActions: bindActionCreators(warningModalActions, dispatch),
        resultActions: bindActionCreators(resultActions, dispatch),
        removeModalActions: bindActionCreators(removeModalActions, dispatch)        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneralInfoPage)