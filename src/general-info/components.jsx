import React from 'react'
import '../css/general-info.css'
import moment from 'moment'
import _ from 'lodash'
import { Row, Col, FormGroup, FormControl, InputGroup, Modal, Button, OverlayTrigger } from 'react-bootstrap'
import { Link } from 'react-router'
import WarningModal from '../utils/components/warning-modal'
import RemoveModal from '../utils/components/remove-modal'
import ConfirmModal from '../utils/components/confirm-modal'
import { tooltip } from '../utils/components/tooltip'

export const GeneralInfoPage = (props) => {
    const { 
        actions, 
        warningModalActions, 
        resultTitle, 
        loading,
        searchName,
        searchDate,
        resultActions,
        removeModalActions,
        showConfirmModal,
        machineChoice
    } = props
    return (
        <div>
            {
                loading ? <LoadingModal loading={loading}/> 
                : <GeneralInfoComponent 
                    actions={actions} 
                    warningModalActions={warningModalActions}
                    resultTitle={resultTitle}
                    resultActions={resultActions}
                    searchName={searchName}
                    searchDate={searchDate}
                    removeModalActions={removeModalActions}
                    showConfirmModal={showConfirmModal}
                    machineChoice={machineChoice}/>
            }
        </div>
    )
}

const GeneralInfoComponent = (props) => {
    const { 
        actions,
        warningModalActions,
        resultTitle,
        searchName,
        searchDate,
        resultActions,
        removeModalActions,
        showConfirmModal,
        machineChoice
    } = props
    return (
        <div id='general-info-page-container'>
            <div id='panel-container'>
                <Row>
                    <Col md={9}>
                        <Col md={6}>
                            <h1>General Info</h1>
                        </Col>
                        <Col md={4} id='detail-container'>
                            <h5>{machineChoice === 'Urinalysis' ? 'Urinalysis Modular System' : 'Automated Hematology System'}</h5>
                        </Col>
                    </Col>
                    <Col md={3}>
                        <Row>
                            <FormGroup>
                                <InputGroup>
                                    <FormControl 
                                        className='search-field' 
                                        type='text' 
                                        placeholder='Search by Name'
                                        name='searchName'
                                        value={searchName}
                                        onChange={(e) => actions.changeSearch(e.target.value, e.target.name)}/>
                                    <InputGroup.Addon><i className='fas fa-search'/></InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <InputGroup>
                                    <FormControl 
                                        className='search-field' 
                                        type='date' 
                                        name='searchDate'
                                        value={searchDate}
                                        onChange={(e) => actions.changeSearch(e.target.value, e.target.name)}/>
                                    <InputGroup.Addon><i className='fas fa-search'/></InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>
                        </Row>
                    </Col>
                </Row>
                <hr/>
                <div id='data-container'>
                    {
                        _.filter(resultTitle, (result) => {return (result.name.toLowerCase()).includes(searchName.toLowerCase()) && result.date.includes(searchDate)})
                        .map((result, key) => {
                            return (
                                <Link key={key} to='result' onClick={() => resultActions.loadResultFromDB(resultTitle[key])} className='data-box'>
                                    <div key={key}>
                                        <p><span className='data-title'>Name:</span> {result.name}</p>
                                        <p><span className='data-title'>Date:</span> {moment(result.date).format('MM / DD / YYYY')}</p>
                                    </div>
                                </Link>
                            )
                        })
                    }
                </div>
            </div>
            <div id='menu-container'>
                <OverlayTrigger placement='left' overlay={tooltip('Delete Data')}>
                    <Button 
                        className='menu-button'
                        disabled={resultTitle.length===0} 
                        onClick={() => removeModalActions.showRemoveModal()}><i className='fa fa-trash-alt'/></Button>
                </OverlayTrigger>
                <OverlayTrigger placement='top' overlay={tooltip('Back to Home')}>
                    <Button 
                        onClick={() => warningModalActions.showWarningModal()} 
                        className='menu-button'
                        style={{marginLeft:'10%', marginRight:'10%'}}>
                        <i className='fa fa-home'/>
                    </Button>
                </OverlayTrigger>
                <OverlayTrigger placement='right' overlay={tooltip('Next to Configuration')}>
                    <Link to='configuration'>
                        <Button className='menu-button'><i className='fa fa-plus'/></Button>
                    </Link>
                </OverlayTrigger>
            </div>
            <WarningModal/>
            <RemoveModal/>
            {showConfirmModal && <ConfirmModal/>}
        </div>
    )            
}

const LoadingModal = (props) => {
    const { loading } = props
    return (
        <Modal show={loading} style={{marginTop: '200px'}}>
            <Modal.Body style={{textAlign: 'center'}}>
                <h1><i className='fas fa-spinner fa-spin'/></h1>
                <h3>Loading Data</h3>
            </Modal.Body>
        </Modal>
    )
}