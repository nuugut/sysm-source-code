import * as types from './consts'

let actions = {}

actions.onChooseMachine = (payload) => {
    return {
        type: types.CHOOSE_MACHINE,
        payload: payload
    }
}

export default actions