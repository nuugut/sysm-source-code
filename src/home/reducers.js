import * as types from './consts'

export default function home(
  state = {
      machineChoice: ''
  }, action) {
    switch (action.type) {
        case types.CHOOSE_MACHINE:
            return {
                ...state,
                machineChoice: action.payload
            }
        default:
            return state
    }
}