import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { HomePage } from './components'
import actions from './actions'

const mapStateToProps = (state) => {
    return {
        machineChoice: state.home.machineChoice
    }
}

const mapDispatchToProps = (dispatch) => {
    return { actions: bindActionCreators(actions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)