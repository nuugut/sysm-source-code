import PouchDB from 'pouchdb'

export default class DB {
    constructor(name) {
        this.db = new PouchDB(name)
    }

    async saveResult(result, header) {
        const response = await this.db.put({ 
            _id: header.id,
            ...result, 
            ...header 
        })
        return response
    }

    async getAllResultTitle(machineChoice) {
        const machine = machineChoice
        const allResult = await this.db.allDocs({ 
            include_docs: true,
            startkey: machine,
            endkey: machine + '\ufff0'
        })
        let resultTitle = []
        allResult.rows.forEach(n => { 
            resultTitle.push({
                id: n.id,
                name: n.doc.name,
                date: n.doc.date,
                machine: n.doc.machine,
                rev: n.value.rev
            })
        })
        return resultTitle
    }

    async deleteDatabase() {
        await this.db.destroy()
    }

    async getResultData(id) {
        const resultData = await this.db.get(id)
        return resultData
    }

    async deleteSingleData(id) {
        const doc = await this.db.get(id)
        const response = await this.db.remove(doc)
        return response
    } 

    async deleteAllData(deletedData) {
        const response = await this.db.bulkDocs(deletedData)
        return response
    }
}