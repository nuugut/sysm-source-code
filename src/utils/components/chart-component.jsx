import React from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts'
import { constructGraphResult } from '../../calculations/common'

export const LineChartResult = (props) => {
    const { resultData, graphType, machine } = props
    const data = constructGraphResult(resultData, graphType, machine)
    return (
        <ResponsiveContainer width='100%' height='100%'>
            <LineChart data={data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey='name' stroke='black' angle={-45} textAnchor='end' interval={0} height={80}tick={{fontSize:'10px', fontWeight:'bold'}}/>
                <YAxis stroke='black' tick={{fontSize:'10px', fontWeight:'bold'}} type='number' domain={[0, 150]} tickCount={10} label={{ value: 'Time ( minutes )', angle:-90, dx:-20}}/>
                <CartesianGrid strokeDasharray='3 3'/>
                <Tooltip/>
                <Line type='linear' dataKey='time' stroke='black'/>
            </LineChart>
        </ResponsiveContainer>
    )
}


export const GroupLineChartResult = (props) => {
    const { resultData, graphType, machine } = props
    const data = constructGraphResult(resultData, graphType, machine)
    return (
        <ResponsiveContainer width='100%' height='100%'>
            <LineChart data={data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey='name' stroke='black' angle={-45} textAnchor='end' interval={0} height={80}tick={{fontSize:'10px', fontWeight:'bold'}}/>
                <YAxis stroke='black' tick={{fontSize:'10px', fontWeight:'bold'}} type='number' domain={[0, 300]} tickCount={20} label={{ value: 'Time ( minutes )', angle:-90, dx:-20}}/>
                <CartesianGrid strokeDasharray='3 3'/>
                <Tooltip/>
                <Line type='linear' dataKey='minimum' stroke='green'/>
                <Line type='linear' dataKey='maximum' stroke='orange'/>
                <Line type='linear' dataKey='average' stroke='blue'/>
            </LineChart>
        </ResponsiveContainer>
    )
}