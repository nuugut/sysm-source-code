import React from 'react'
import { Tooltip } from 'react-bootstrap'

export const tooltip = (value) => {
    return (
        <Tooltip id="tooltip">
            {value}
        </Tooltip>
    )
}