import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import actions from '../actions/warning-modal-actions'

export const WarningModal = (props) => {
    const { actions, showWarningModal } = props
    return (
        <Modal show={showWarningModal} style={{marginTop: '200px'}} onHide={() => actions.closeWarningModal()}>
            <Modal.Body style={{textAlign: 'center'}}>
                <h1><i className='fas fa-exclamation-circle' style={{color:'#c9302c'}}/></h1>
                <h3>Back to home will reset all data. <br/>Would you like to continue?</h3>
            </Modal.Body>
            <Modal.Footer style={{textAlign: 'center'}}>
                <Link to='home'>
                    <Button 
                        style={{fontWeight: 'bold', marginRight: '5px', width: '40%'}} 
                        onClick={() => {actions.closeWarningModal(); actions.resetAllData()}}>Yes</Button>
                </Link>
                <Button 
                    style={{fontWeight: 'bold', width: '40%'}} 
                    onClick={() => actions.closeWarningModal()}>No</Button>
            </Modal.Footer>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        showWarningModal: state.utils.showWarningModal,
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WarningModal)