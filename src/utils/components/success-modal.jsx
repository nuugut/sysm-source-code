import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import actions from '../actions/success-modal-actions'

export const SuccessModal = (props) => {
    const { actions, showSuccessModal, resultName, resultDate } = props
    return (
        <Modal show={showSuccessModal} style={{marginTop: '200px'}} onHide={() => actions.closeSuccessModal()}>
            <Modal.Body style={{textAlign: 'center'}}>
                <h1><i className='fas fa-check-circle' style={{color:'#5cb85c'}}/></h1>
                <h3>Save Data</h3>
                <h3>Name: {resultName}</h3>
                <h3>Date: {resultDate}</h3>                
                <h3>Successfully</h3>
            </Modal.Body>
            <Modal.Footer style={{textAlign: 'center'}}>
                <Button 
                    style={{fontWeight: 'bold', width: '40%'}} 
                    onClick={() => actions.closeSuccessModal()}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        showSuccessModal: state.utils.showSuccessModal,
        resultName: state.configuration.name,
        resultDate: state.configuration.date
    }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SuccessModal)