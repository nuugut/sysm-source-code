import * as types from './consts'

export default function utils(
  state = {
      showWarningModal: false,
      showSuccessModal: false,
      showRemoveModal: false,
      showConfirmModal: {
          show: false,
          key: ''
      }
  }, action) {
    switch (action.type) {
        case types.SHOW_WARNING_MODAL:
            return {
                ...state,
                showWarningModal: true
            }
        case types.CLOSE_WARNING_MODAL:
            return {
                ...state,
                showWarningModal: false
            }
        case types.SHOW_SUCCESS_MODAL:
            return {
                ...state,
                showSuccessModal: true
            }
        case types.CLOSE_SUCCESS_MODAL:
            return {
                ...state,
                showSuccessModal: false
            }
        case types.SHOW_REMOVE_MODAL:
            return {
                ...state,
                showRemoveModal: true
            }
        case types.CLOSE_REMOVE_MODAL:
            return {
                ...state,
                showRemoveModal: false
            }
        case types.SHOW_CONFIRM_MODAL:
            return {
                ...state,
                showConfirmModal: {
                    show: true,
                    key: action.payload
                }
            }
        case types.CLOSE_CONFIRM_MODAL:
            return {
                ...state,
                showConfirmModal: {
                    show: false,
                    key: ''
                }
            }
        default:
            return state
    }
}