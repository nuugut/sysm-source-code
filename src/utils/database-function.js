import DB from '../db'

export const saveResult = async (result, configuration, machineChoice) => {
    const db = new DB('sysm-db')
    const id = machineChoice + '_' + guid()    
    const header = {
        name: configuration.name,
        date: configuration.date,
        machine: machineChoice,
        id: id
    }
    const response = await db.saveResult(result, header)
}

export const getAllResultTitle = async (machineChoice) => {
    const db = new DB('sysm-db')
    const result = await db.getAllResultTitle(machineChoice)
    return result
}

export const deleteDatabase = async () => {
    const db = new DB('sysm-db')
    db.deleteDatabase()
}

export const getResultData = async (resultTitle) => {
    const db = new DB('sysm-db')
    const result = await db.getResultData(resultTitle.id)
    return result
}

export const deleteSingleData = async (resultTitle) => {
    const db = new DB('sysm-db')
    const response = await db.deleteSingleData(resultTitle.id)
    return response
}

export const deleteAllData = async (resultTitle) => {
    const deletedData = []
    resultTitle.forEach((item) => {
        deletedData.push({
            _rev: item.rev,
            _deleted: true,
            _id: item.id
        })
    })
    const db = new DB('sysm-db')
    const response = await db.deleteAllData(deletedData)
    return response
}

const guid = () => {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}