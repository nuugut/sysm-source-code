import * as types from '../consts'

let actions = {}

actions.showSuccessModal = () => {
    return { type: types.SHOW_SUCCESS_MODAL }
}

actions.closeSuccessModal = () => {
    return { type: types.CLOSE_SUCCESS_MODAL }
}

export default actions