import * as types from '../consts'

let actions = {}

actions.showConfirmModal = (key) => {
    return { 
        type: types.SHOW_CONFIRM_MODAL, 
        payload: key 
    }
}

actions.closeConfirmModal = () => {
    return { type: types.CLOSE_CONFIRM_MODAL }
}


export default actions