import * as types from './consts'

let actions = {}

actions.changeInput = (payload, key) => {
    return {
        type: types.CHANGE_INPUT,
        payload: payload,
        key: key
    }
}

actions.changeConfig = (payload, index, key) => {
    return {
        type: types.CHANGE_CONFIG,
        payload: payload,
        index: index,
        key: key
    }
}

actions.resetData = () => {
    console.log('RESET_CONFIGURATION_DATA')
    return {
        type: types.RESET_DATA
    }
}

export default actions